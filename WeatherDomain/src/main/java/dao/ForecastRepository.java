package dao;

import entity.ForecastEntity;
import exceptions.EntityNotFoundException;

/**
 * Created by rob on 19/03/17.
 */
public interface ForecastRepository {

    void saveWeatherEntity(ForecastEntity entity);
    ForecastEntity findWeatherEntity(String location, long date) throws EntityNotFoundException;
    String getDbType();
}
