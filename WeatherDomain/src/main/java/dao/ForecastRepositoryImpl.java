package dao;

import entity.ForecastEntity;
import exceptions.EntityNotFoundException;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;

/**
 * Created by rob on 19/03/17.
 */
@Named
@RequestScoped
@Default
public class ForecastRepositoryImpl implements ForecastRepository {

    private String dbType = "JPA";

    @PersistenceContext(unitName = "ForecastPUnit")
    EntityManager em;

    @Override
    public void saveWeatherEntity(ForecastEntity entity) {
        em.persist(entity);
    }

    @Override
    public ForecastEntity findWeatherEntity(String location, long date) throws EntityNotFoundException {
        String queryString = "SELECT f FROM ForecastEntity f WHERE f.location=(:loc) AND f.dt=(:dt)";
        Query query = em.createQuery(queryString);
        query.setParameter("loc", location);
        query.setParameter("dt", date);

        ForecastEntity entity = (ForecastEntity) query.getResultList().get(0);

        if (entity == null){
            throw new EntityNotFoundException();
        }
        return entity;
    }

    public String getDbType() {
        return dbType;
    }

    final static class DateLocation {
        private static Date date;
        private static String location;

        private DateLocation(Date date, String location){
            setDate(date);
            setLocation(location);
        }

        public static DateLocation of(Date date, String location){
            return new DateLocation(date, location);
        }

        public boolean equals(Object o){
            if (o instanceof DateLocation){
                DateLocation dateLocation = (DateLocation)o;
                return (dateLocation.getDate().equals(this.getDate()) && dateLocation.getLocation().equals(this.getLocation()));

            }
            return false;
        }

        public static Date getDate() {
            return date;
        }

        public static String getLocation() {
            return location;
        }

        private static void setDate(Date date) {
            DateLocation.date = date;
        }

        private static void setLocation(String location) {
            DateLocation.location = location;
        }
    }
}
