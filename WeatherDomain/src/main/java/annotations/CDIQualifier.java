package annotations;

import javax.inject.Qualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Rob on 26/03/2017.
 */
@Qualifier
@Retention(RUNTIME) @Target({TYPE, METHOD, FIELD, PARAMETER})
public @interface CDIQualifier {
}
