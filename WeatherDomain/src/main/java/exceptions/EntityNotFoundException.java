package exceptions;

/**
 * Created by Rob on 25/03/2017.
 */
public class EntityNotFoundException extends Exception {

    public EntityNotFoundException(){
        super();
    }

    public EntityNotFoundException(String message){
        super(message);
    }
}
