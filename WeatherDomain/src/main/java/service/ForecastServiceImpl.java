package service;

import dao.ForecastRepository;
import entity.ForecastEntity;
import exceptions.EntityNotFoundException;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by rob on 19/03/17.
 */
@Named
@RequestScoped
@Default
public class ForecastServiceImpl implements ForecastService {

    @Inject
    private ForecastRepository forecastRepository;

    @Override
    public void saveForecast(ForecastEntity entity) {
        forecastRepository.saveWeatherEntity(entity);
    }

    @Override
    public ForecastEntity findForecast(String location, long date) throws EntityNotFoundException {
        return forecastRepository.findWeatherEntity(location, date);
    }

    @Override
    public String findDbType() {
        return forecastRepository.getDbType();
    }
}
