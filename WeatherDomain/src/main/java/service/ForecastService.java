package service;

import entity.ForecastEntity;
import exceptions.EntityNotFoundException;

/**
 * Created by rob on 19/03/17.
 */
public interface ForecastService {

    void saveForecast(ForecastEntity entity);
    ForecastEntity findForecast(String location, long date) throws EntityNotFoundException;
    String findDbType();
}
