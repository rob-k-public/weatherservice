package entity;

import entity.data.ForecastCloudEntity;
import entity.data.ForecastDetailsEntity;
import entity.data.ForecastWeatherEntity;
import entity.data.ForecastWindEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by Rob on 25/03/2017.
 */
@Entity
public class ForecastEntity implements Serializable {

    @Id
    private String location;
    @Id
    private long dt;
    private Date moment;
    private String dt_txt;

    @OneToOne(targetEntity = ForecastDetailsEntity.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private ForecastDetailsEntity main;

    @OneToOne(targetEntity = ForecastWeatherEntity.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<ForecastWeatherEntity> weather = Collections.emptyList();

    @OneToOne(targetEntity = ForecastWindEntity.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private ForecastWindEntity wind;

    @OneToOne(targetEntity = ForecastCloudEntity.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private ForecastCloudEntity clouds;


    public ForecastEntity() {

    }

    public ForecastDetailsEntity getMain() {
        return main;
    }

    public void setMain(ForecastDetailsEntity main) {
        this.main = main;
    }

    public List<ForecastWeatherEntity> getWeather() {
        return weather;
    }

    public void setWeather(List<ForecastWeatherEntity> weather) {
        this.weather = weather;
    }

    public ForecastCloudEntity getClouds() {
        return clouds;
    }

    public void setClouds(ForecastCloudEntity clouds) {
        this.clouds = clouds;
    }

    public ForecastWindEntity getWind() {
        return wind;
    }

    public void setWind(ForecastWindEntity wind) {
        this.wind = wind;
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getMoment() {
        return moment;
    }

    public void setMoment(Date moment) {
        this.moment = moment;
    }
}
