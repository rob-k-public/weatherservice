package entity.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Rob on 25/03/2017.
 */
@Entity
public class ForecastCloudEntity {

    @Id
    @GeneratedValue
    private long id;

    private String all;

    public String getAll() {
        return all;
    }

    public void setAll(String all) {
        this.all = all;
    }
}
