package dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Created by Rob on 26/03/2017.
 */
@RunWith(JUnit4.class)
public class ForecastRepositoryImplTest {

    @Test
    public void twoDateLocationsMatch(){
        Date date = new Date(01,01,2017);
        Date date1 = new Date(01,01,2017);
        assertEquals(date, date1);
        ForecastRepositoryImpl.DateLocation firstDate = ForecastRepositoryImpl.DateLocation.of(date, "Westerlo");
        ForecastRepositoryImpl.DateLocation secondDate = ForecastRepositoryImpl.DateLocation.of(date1, "Westerlo");
        assertEquals(firstDate, secondDate);
    }
}