package mapper;

import dto.ForecastDto;
import dto.WeatherDetailsDto;
import entity.ForecastEntity;
import entity.data.ForecastCloudEntity;
import entity.data.ForecastDetailsEntity;
import entity.data.ForecastWeatherEntity;
import entity.data.ForecastWindEntity;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Rob on 25/03/2017.
 */
public class ForecastMapper {

    public ForecastEntity mapToEntity(ForecastDto dto, String location){
        ForecastEntity entity = new ForecastEntity();

        entity.setLocation(location);
        entity.setDt(Long.parseLong(dto.getDt()));
        entity.setMoment(Date.from(Instant.ofEpochSecond(Long.parseLong(dto.getDt()))));
        entity.setDt_txt(dto.getDt_txt());

        //FORECASTDETAILS
        ForecastDetailsEntity detailsEntity = new ForecastDetailsEntity();
        detailsEntity.setTemp(Double.parseDouble(dto.getMain().getTemp()));
        detailsEntity.setTemp_min(Double.parseDouble(dto.getMain().getTemp_min()));
        detailsEntity.setTemp_max(Double.parseDouble(dto.getMain().getTemp_max()));
        detailsEntity.setPressure(Double.parseDouble(dto.getMain().getPressure()));
        detailsEntity.setHumidity(Double.parseDouble(dto.getMain().getHumidity()));
        entity.setMain(detailsEntity);

        //FORECASTWEATHER
        List<ForecastWeatherEntity>entities = new ArrayList<>();
        for(WeatherDetailsDto detdto : dto.getWeather()) {
            ForecastWeatherEntity weatherEntity = new ForecastWeatherEntity();
            weatherEntity.setId(detdto.getId());
            weatherEntity.setMain(detdto.getMain());
            weatherEntity.setDescription(detdto.getDescription());
            entities.add(weatherEntity);
        }
        entity.setWeather(entities);

        //FORECASTCLOUDS
        ForecastCloudEntity cloudEntity = new ForecastCloudEntity();
        cloudEntity.setAll(dto.getClouds().getAll());
        entity.setClouds(cloudEntity);

        //FORECASTWIND
        ForecastWindEntity windEntity = new ForecastWindEntity();
        windEntity.setSpeed(Double.parseDouble(dto.getWind().getSpeed()));
        windEntity.setDeg(Double.parseDouble(dto.getWind().getDeg()));
        entity.setWind(windEntity);

        return entity;
    }
}
