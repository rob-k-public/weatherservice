package feed;

import com.fasterxml.jackson.databind.ObjectMapper;
import dto.WeatherDto;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * Created by rob on 19/03/17.
 */
public class WeatherConsumer {

    private Client client;
    private  static  String url = "http://api.openweathermap.org/data/2.5/weather";

    public String getWeatherJSON(String location){
        client = ClientBuilder.newClient();
        WebTarget feedTarget = client.target(url)
                .queryParam("q", location)
                .queryParam("units", "metric")
                .queryParam("appid", "5dc39cb5de02165e67b74a4d2999448a");
        Response response = feedTarget.request(MediaType.APPLICATION_JSON).get();
        return response.readEntity(String.class);
    }

    public WeatherDto getWeather(String json) throws IOException {
        WeatherDto weatherDto = new ObjectMapper().readValue(json, WeatherDto.class);
        return  weatherDto;
    }

}
