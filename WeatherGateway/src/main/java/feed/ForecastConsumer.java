package feed;

import com.fasterxml.jackson.databind.ObjectMapper;
import dto.ForecastDto;
import dto.ForecastFeedDto;
import mapper.ForecastMapper;
import service.ForecastService;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * Created by Rob on 25/03/2017.
 */
@EJB
public class ForecastConsumer {

    @Inject
    private ForecastService forecastService;

    private Client client;
    private static String url = "http://api.openweathermap.org/data/2.5/forecast";
    private ForecastMapper mapper = new ForecastMapper();

    public String getWeatherJSON(String location) {
        client = ClientBuilder.newClient();
        WebTarget feedTarget = client.target(url)
                .queryParam("q", location)
                .queryParam("units", "metric")
                .queryParam("appid", "5dc39cb5de02165e67b74a4d2999448a");
        Response response = feedTarget.request(MediaType.APPLICATION_JSON).get();
        return response.readEntity(String.class);
    }

    public ForecastFeedDto getForecast(String json) throws IOException {
        ForecastFeedDto feedDto = new ObjectMapper().readValue(json, ForecastFeedDto.class);
        return feedDto;
    }

    @Schedule(hour = "12")
    public void consumeFeed() throws IOException {
        ForecastFeedDto feedDto = getForecast(getWeatherJSON("Westerlo,be"));
        for (ForecastDto dto : feedDto.getList()) {
            forecastService.saveForecast(mapper.mapToEntity(dto, feedDto.getCity().getName() + "," + feedDto.getCity().getCountry()));
        }
    }
}
