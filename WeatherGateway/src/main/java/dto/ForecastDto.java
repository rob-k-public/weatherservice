package dto;

import java.util.Collections;
import java.util.List;

/**
 * Created by Rob on 25/03/2017.
 */
public class ForecastDto {

    private String dt;
    private ForecastMainDto main;
    private List<WeatherDetailsDto> weather = Collections.emptyList();
    private CloudDetailsDto clouds;
    private WindDetailsDto wind;
    private ForecastSysDetailsDto sys;
    private String dt_txt;

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public ForecastMainDto getMain() {
        return main;
    }

    public void setMain(ForecastMainDto main) {
        this.main = main;
    }

    public List<WeatherDetailsDto> getWeather() {
        return weather;
    }

    public void setWeather(List<WeatherDetailsDto> weather) {
        this.weather = weather;
    }

    public CloudDetailsDto getClouds() {
        return clouds;
    }

    public void setClouds(CloudDetailsDto clouds) {
        this.clouds = clouds;
    }

    public WindDetailsDto getWind() {
        return wind;
    }

    public void setWind(WindDetailsDto wind) {
        this.wind = wind;
    }

    public ForecastSysDetailsDto getSys() {
        return sys;
    }

    public void setSys(ForecastSysDetailsDto sys) {
        this.sys = sys;
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }
}
