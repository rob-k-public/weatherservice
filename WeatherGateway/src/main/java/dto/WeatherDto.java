package dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rob on 19/03/17.
 */
public class WeatherDto {

    private CoordDetailsDto coord;
    private List<WeatherDetailsDto> weather = new ArrayList<>();
    private  String base;
    private WeatherItemDto main;
    private double visibility;
    private WindDetailsDto wind;
    private CloudDetailsDto clouds;
    private String dt;
    private SysDetailsDto sys;
    private String id;
    private String name;
    private String cod;

    public CoordDetailsDto getCoord() {
        return coord;
    }

    public void setCoord(CoordDetailsDto coord) {
        this.coord = coord;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public WeatherItemDto getMain() {
        return main;
    }

    public void setMain(WeatherItemDto main) {
        this.main = main;
    }

    public double getVisibility() {
        return visibility;
    }

    public void setVisibility(double visibility) {
        this.visibility = visibility;
    }

    public WindDetailsDto getWind() {
        return wind;
    }

    public void setWind(WindDetailsDto wind) {
        this.wind = wind;
    }

    public CloudDetailsDto getClouds() {
        return clouds;
    }

    public void setClouds(CloudDetailsDto clouds) {
        this.clouds = clouds;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public SysDetailsDto getSys() {
        return sys;
    }

    public void setSys(SysDetailsDto sys) {
        this.sys = sys;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public List<WeatherDetailsDto> getWeather() {
        return weather;
    }

    public void setWeather(List<WeatherDetailsDto> weather) {
        this.weather = weather;
    }
}
