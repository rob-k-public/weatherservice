package dto;

import java.util.Collections;
import java.util.List;

/**
 * Created by Rob on 25/03/2017.
 */
public class ForecastFeedDto {

    private CityDetailsDto city;
    private String cod;
    private String message;
    private String cnt;
    private List<ForecastDto> list = Collections.emptyList();

    public ForecastFeedDto(){

    }

    public CityDetailsDto getCity() {
        return city;
    }

    public void setCity(CityDetailsDto city) {
        this.city = city;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCnt() {
        return cnt;
    }

    public void setCnt(String cnt) {
        this.cnt = cnt;
    }

    public List<ForecastDto> getList() {
        return list;
    }

    public void setList(List<ForecastDto> list) {
        this.list = list;
    }
}
