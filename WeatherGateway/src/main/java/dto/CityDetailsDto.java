package dto;

/**
 * Created by Rob on 25/03/2017.
 */
public class CityDetailsDto {

    private String id;
    private String name;
    private CoordDetailsDto coord;
    private String country;
    private String population;
    private SysDetailsDto sys;

    public CityDetailsDto(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CoordDetailsDto getCoord() {
        return coord;
    }

    public void setCoord(CoordDetailsDto coord) {
        this.coord = coord;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public SysDetailsDto getSys() {
        return sys;
    }

    public void setSys(SysDetailsDto sys) {
        this.sys = sys;
    }
}
