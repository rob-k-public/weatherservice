package dto;

/**
 * Created by Rob on 25/03/2017.
 */
public class ForecastSysDetailsDto {

    private String pod;

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }
}
