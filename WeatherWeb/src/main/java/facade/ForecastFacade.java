package facade;

import dto.ForecastDto;
import exceptions.EntityNotFoundException;

/**
 * Created by Rob on 25/03/2017.
 */
public interface ForecastFacade {

    ForecastDto findForecast(long date, String location) throws EntityNotFoundException;
    String findDbType();
}
