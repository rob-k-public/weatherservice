package facade;

import dto.ForecastDto;
import entity.ForecastEntity;
import exceptions.EntityNotFoundException;
import mapper.ForecastMapper;
import service.ForecastService;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Rob on 25/03/2017.
 */
@Named
@RequestScoped
@Default
public class ForecastFacadeImpl implements ForecastFacade {

    @Inject
    private ForecastService forecastService;

    private ForecastMapper mapper = new ForecastMapper();

    @Override
    public ForecastDto findForecast(long date, String location) throws EntityNotFoundException {
        ForecastEntity entity = forecastService.findForecast(location, date);
        return mapper.mapToDto(entity);
    }

    @Override
    public String findDbType() {
        return forecastService.findDbType();
    }
}
