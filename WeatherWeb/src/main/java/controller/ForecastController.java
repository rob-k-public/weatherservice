package controller;

import dto.ForecastDto;
import exceptions.EntityNotFoundException;
import facade.ForecastFacade;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * Created by Rob on 25/03/2017.
 */
@Path("/forecast")
@Named
@RequestScoped
public class ForecastController {

    @Inject
    private ForecastFacade forecastFacade;

    @GET
    @Path("/health")
    public String getHealth() {
        String serverStatus = "Status: online \n";
        String dbType = "Database: " + forecastFacade.findDbType() + "\n";
        return serverStatus + dbType;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ForecastDto find(@QueryParam("dt")long dt, @QueryParam("location")String location) throws EntityNotFoundException {
       return  forecastFacade.findForecast(dt, location);
    }
}
